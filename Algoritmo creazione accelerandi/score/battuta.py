import nota
import metro
import pdb
class Battuta:
	def __init__(self,num,den,note,pulse =4):
		self.note = note
		self.num = num
		self.den = den
		self.pulse = pulse
		self.den_abs = self.pulse/self.den
	 
	def next_off(self,metro,n):
		now = n 
		#pdb.set_trace()
		for t in range(self.num):
			num_abs = (t+1)/self.den_abs
			tempo = metro.cur_beat(now)
			now += num_abs*(60/tempo)
		return now

	def output(self,metro,n):
		now = n
		for n in self.note:
			mperiod = metro.to_mperiod(now)
			n.output(mperiod) 	
		return self.next_off(metro,now)
	



