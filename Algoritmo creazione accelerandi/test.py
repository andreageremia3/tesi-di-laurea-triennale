import sys
sys.path.append("./score")
from score import *
m = Metro(0,20,72,72) #start dell'accelerando, durata, bpm_st,bpm_end
s = Suono("zio.wav",0.75) #file path e durata del file
seq = Sequenza(m,
    [
        Battuta(4,4,[
            Nota(Ritmo(1,4), s),Nota(Ritmo(2,4), s),Nota(Ritmo(3,4), s),Nota(Ritmo(4,4), s)]),
        Battuta(4,4,[
            Nota(Ritmo(1,4), s),Nota(Ritmo(2,4), s),Nota(Ritmo(3,4), s),Nota(Ritmo(4,4), s)])
    ])

seq.output()